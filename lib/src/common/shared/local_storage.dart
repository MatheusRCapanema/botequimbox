import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LocalFlutterSecureStorage {
  final FlutterSecureStorage _flutterSecureStorage;

  LocalFlutterSecureStorage(this._flutterSecureStorage);

  Future<String?> get(String key) async {
    return await _flutterSecureStorage.read(key: key);
  }

  Future<Map<String, String>> getAllValues() async {
    return await _flutterSecureStorage.readAll();
  }

  Future<void> delete(String key) async {
    await _flutterSecureStorage.delete(key: key);
  }

  Future<void> deleteAll() async {
    await _flutterSecureStorage.deleteAll();
  }

  Future<void> save(String key, String? value) async {
    await _flutterSecureStorage.write(key: key, value: value);
  }
}

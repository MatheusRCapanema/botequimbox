import 'package:botequimbox/src/common/size_config.dart';
import 'package:botequimbox/src/common/style_constants.dart';
import 'package:flutter/material.dart';

import '../feature/home/common/dto/product_dto.dart';

class RoundedIconButton extends StatelessWidget {
  const RoundedIconButton({
    super.key, required this.iconData, required this.press, 
     this.showShadow = false,
  });

 final IconData iconData;
  final GestureTapCancelCallback press;
  final bool showShadow;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getProportionateScreenWidth(30),
      width: getProportionateScreenWidth(40),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          if (showShadow)
            BoxShadow(
              offset: Offset(0, 6),
              blurRadius: 10,
              color: Color(0xFFB0B0B0).withOpacity(0.2),
            ),
        ],
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
          primary: oPrimaryColor,
          backgroundColor: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        ),
        onPressed: press,
        child: Icon(iconData),
      ),
    );
  }
}


class ProductDetailsArgument{
  final ProductDto product;

  ProductDetailsArgument({required this.product});
}
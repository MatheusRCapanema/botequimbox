import 'package:flutter/material.dart';

import 'size_config.dart';
import 'style_constants.dart';

class DefaultButton extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final bool disabled;

  const DefaultButton({
    Key? key,
    required this.text,
    required this.press,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(56),
      child: TextButton(
        onPressed: disabled ? null : press,
        style: TextButton.styleFrom(
          backgroundColor: disabled ? oPrimaryLightColor : oPrimaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        child: Text(
          text,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
            color: Colors.white,
            fontFamily: "Muli",
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

const oPrimaryColor = Color.fromARGB(255, 139, 51, 255);
const oPrimaryLightColor = Color.fromARGB(255, 223, 187, 253);
const oPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color.fromARGB(255, 104, 62, 255), Color.fromARGB(255, 89, 0, 206)],
);

const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

// Form Error
final RegExp cnpjValidatorRegExp =
    RegExp(r"[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}");
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kCnpjNullError = "Please Enter your CNPJ";
const String kInvalidCnpjError = "Please Enter Valid CNPJ";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";

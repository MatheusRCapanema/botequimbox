class Constants {
  Constants._();

  // ONBOARDING CONSTANTS
  static const String ourConditionKey = "ourCondition";

  // AUTHENTICATION CONSTANTS
  static const String accessTokenKey = "accessToken";
  static const String refreshTokenKey = "refreshToken";
}

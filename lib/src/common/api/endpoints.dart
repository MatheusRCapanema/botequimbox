class Endpoints {
  Endpoints._();

  // base url
  static const String baseUrl = "https://botequimbox-backend.onrender.com/api";

  // receiveTimeout
  static const Duration receiveTimeout = Duration(seconds: 45);

  // connectTimeout
  static const Duration connectionTimeout = Duration(seconds: 45);

  static const String authV1 = '/v1/auth';
  static const String usersV1 = '/v1/users';
}

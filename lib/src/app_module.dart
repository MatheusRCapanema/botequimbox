import 'package:botequimbox/src/common/shared/local_storage.dart';
import 'package:botequimbox/src/feature/home/home_module.dart';
import 'package:botequimbox/src/feature/onboarding/onboarding_module.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'common/api/dio_client.dart';
import 'feature/auth/auth_module.dart';
import 'feature/splash/splash_module.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((dio) => Dio()),
        Bind.singleton((dioClient) => DioClient(dioClient())),
        Bind.singleton((flutterSecureStorage) => const FlutterSecureStorage()),
        Bind.singleton(
          (flutterSecureStorage) => LocalFlutterSecureStorage(
            flutterSecureStorage(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ModuleRoute(Modular.initialRoute, module: SplashModule()),
        ModuleRoute('/onboarding', module: OnboardingModule()),
        ModuleRoute('/auth', module: AuthModule()),
        ModuleRoute('/home', module: HomeModule()),
      ];
}

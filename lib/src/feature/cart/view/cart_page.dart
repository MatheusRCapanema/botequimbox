import "package:botequimbox/src/feature/cart/widget/body.dart";

import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:flutter_modular/flutter_modular.dart";

import "../../../common/size_config.dart";
import "../model/cart.dart";
import "../widget/check_out_card.dart";

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: const Body(),
      bottomNavigationBar: const CheckoutCard(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: BackButton(
        color: Colors.black,
        onPressed: () {
          Modular.to.navigate('/home');
        },
      ),
      titleTextStyle: TextStyle(
          color: const Color(0XFF8B8B8B),
          fontSize: getProportionateScreenWidth(15),
          fontFamily: "Muli"),
      title: Column(
        // Remove 'const' from here
        children: [
          const Text(
            "Carrinho",
          ),
          Text(
            "${demoCarts.length} items",
          ),
        ],
      ),
      centerTitle: true,
    );
  }
}

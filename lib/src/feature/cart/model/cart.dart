import 'package:botequimbox/src/feature/home/common/dto/product_dto.dart';
import '../../home/common/api/products_mock.dart';

class Cart {
  final ProductDto product;
  final int numOfItem;

  Cart({required this.product, required this.numOfItem});
}

List<Cart> demoCarts = [
  Cart(product: ProductsMock.demoProducts[0], numOfItem: 2),
  Cart(product: ProductsMock.demoProducts[1], numOfItem: 1),
  Cart(product: ProductsMock.demoProducts[3], numOfItem: 1),
];

double getTotalCost(List<Cart> carts) {
  double total = 0;
  for (Cart cart in carts) {
    total += cart.product.price! * cart.numOfItem;
  }
  return total;
}

import 'package:botequimbox/src/feature/details/view/page/details_page.dart';
import 'package:botequimbox/src/feature/home/common/dto/product_dto.dart';
import 'package:botequimbox/src/feature/home/usecase/product_usecase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/rounded_icon_button.dart';
import '../../../../common/size_config.dart';
import 'product_card.dart';
import 'section_title.dart';

class PopularProducts extends StatefulWidget {
  const PopularProducts({super.key});

  @override
  _PopularProductsState createState() => _PopularProductsState();
}

class _PopularProductsState extends State<PopularProducts> {
  final ProductUseCase _productUseCase = Modular.get<ProductUseCase>();
  List<ProductDto> _popularProducts = List.empty();

  @override
  void initState() {
    _getPopularProducts();
    super.initState();
  }

  Future<void> _getPopularProducts() async {
    final popularProducts = await _productUseCase.getProducts();
    setState(() {
      _popularProducts = popularProducts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: SectionTitle(text: "Popular Products", press: () {}),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                _popularProducts.length,
                (index) {
                  if (_popularProducts.isNotEmpty) {
                    if (_popularProducts[index].isPopular!) {
                      return ProductCard(
                        product: _popularProducts[index],
                        press: () => Modular.to.pushNamed('/home/details',
                        arguments: ProductDetailsArgument(product:  _popularProducts[index],
                            )
                          )
                        );
                    }
                  }

                  return const SizedBox.shrink();
                },
              ),
              SizedBox(width: getProportionateScreenWidth(20)),
            ],
          ),
        )
      ],
    );
  }
}

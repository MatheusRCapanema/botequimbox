import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/size_config.dart';
import 'notification_button.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SearchField(),
          NotificationButton(
            svgSrc: "lib/assets/icons/Cart Icon.svg",
            press: () => Modular.to.navigate('/home/cart'),
          ),
          NotificationButton(
            svgSrc: "lib/assets/icons/Bell.svg",
            nItems: 3,
            press: () {},
          ),
        ],
      ),
    );
  }
}

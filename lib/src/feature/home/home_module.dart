import 'package:botequimbox/src/feature/cart/view/cart_page.dart';
import 'package:botequimbox/src/feature/home/common/api/product_api.dart';
import 'package:botequimbox/src/feature/home/repository/product_repository.dart';
import 'package:botequimbox/src/feature/home/usecase/product_usecase.dart';
import 'package:botequimbox/src/feature/home/view/page/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../details/view/page/details_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.factory((productApi) => ProductApi()),
    Bind.factory((productRepository) => ProductRepository()),
    Bind.factory((productUseCase) => ProductUseCase()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, __) => const HomePage()),
    ChildRoute(
      '/details',
      child: (BuildContext context, args) => const DetailsPage(),
      children: [],
    ),

        ChildRoute(
      '/cart',
      child: (BuildContext context, args) => const CartPage(),
      children: [],
    ),

  ];
}

import '../dto/product_dto.dart';

class ProductsMock {
  static const String description =
      "Bebida Gaseificada no sabor caramelo, a coca-cola traz a alegria que sua família merece, com grandes 3L sua família…";

  static List<ProductDto> demoProducts = [
    ProductDto(
      id: 1,
      images: [
        "lib/assets/images/coca_cola_1.png",
        "lib/assets/images/coca_cola_2.webp",
      ],
      title: "Coca Cola - Bebida Caramelizada",
      price: 12.99,
      description: description,
      rating: 4.8,
      isFavourite: true,
      isPopular: true,
            size:[
        "300ml",
        "400ml",
        "1L",
        "1,5L",
      ],
    ),
    ProductDto(
      id: 2,
      images: [
        "lib/assets/images/monster.webp",
      ],
      title: "Monster - Energético Açucarado",
      price: 8.99,
      description: description,
      rating: 4.1,
      isPopular: true,
      size:[
        "Vibe",
        "Fusion",
        "Trad",
        "Mango",
      ],
    ),
    ProductDto(
      id: 3,
      images: [
        "lib/assets/images/fanta_laranja.webp",
      ],
      title: "Fanta Laranja - Bebida Gaseificada",
      price: 5.49,
      description: description,
      rating: 4.1,
      isFavourite: true,
      isPopular: true,
            size:[
        "1L",
        "1,5L",
        "2L",
        "3L",
      ],
    ),
    ProductDto(
      id: 4,
      images: [
        "lib/assets/images/agua.webp",
      ],
      title: "Água Mineral S/ Gás",
      price: 1.89,
      description: description,
      rating: 4.1,
      isFavourite: true,
      isPopular: true,
            size:[
        "250ml",
        "500ml",
        "1,5L",
        "2L",
      ]
    ),
  ];
}

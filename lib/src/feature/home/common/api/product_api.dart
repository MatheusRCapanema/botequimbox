import 'package:botequimbox/src/feature/home/common/api/products_mock.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/api/dio_client.dart';

class ProductApi {
  final DioClient _dioClient = Modular.get<DioClient>();

  Future<Response> getProducts() async {
    try {
      late Response response = Response(requestOptions: RequestOptions());
      response.data = ProductsMock.demoProducts;
      response.statusCode = 200;

      return response;
    } catch (e) {
      rethrow;
    }
  }
}

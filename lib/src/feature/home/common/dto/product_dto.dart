import 'package:flutter/material.dart';

class ProductDto {
  int? id;
  String? title, description;
  List<String>? images;
  double? rating, price;
  bool? isFavourite, isPopular;
  List<String>? size;  // size é agora uma lista de strings

  ProductDto({
    required this.id,
    required this.images,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description,
    required this.size,
  });

  ProductDto.fromDto(ProductDto product) {
    id = product.id;
    images = product.images;
    title = product.title;
    price = product.price;
    description = product.description;
    rating = product.rating;
    isFavourite = product.isFavourite;
    isPopular = product.isPopular;
    size = product.size;
  }

  ProductDto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    images = List<String>.from(json['images']);
    title = json['title'];
    price = json['price'].toDouble();
    rating = json['rating'].toDouble();
    description = json['description'];
    isFavourite = json['isFavourite'];
    isPopular = json['isPopular'];
    size = List<String>.from(json['size']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['images'] = images;
    data['title'] = title;
    data['price'] = price;
    data['rating'] = rating;
    data['isFavourite'] = isFavourite;
    data['isPopular'] = isPopular;
    data['description'] = description;
    data['size'] = size;

    return data;
  }
}

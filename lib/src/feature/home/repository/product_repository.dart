import 'package:botequimbox/src/feature/home/common/api/product_api.dart';
import 'package:botequimbox/src/feature/home/common/dto/product_dto.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../common/api/dio_exception.dart';

class ProductRepository {
  final ProductApi _productApi = Modular.get<ProductApi>();

  Future<List<ProductDto>> getProducts() async {
    try {
      final response = await _productApi.getProducts();
      if (response.statusCode != 200) {
        // @TODO Alert Bad credentials
      }

      return response.data;
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      throw errorMessage;
    }
  }
}

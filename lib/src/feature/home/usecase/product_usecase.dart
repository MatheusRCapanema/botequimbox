import 'package:botequimbox/src/feature/home/repository/product_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../common/dto/product_dto.dart';

class ProductUseCase {
  final ProductRepository _productRepository = Modular.get<ProductRepository>();

  Future<List<ProductDto>> getProducts() async {
    final List<ProductDto> products = await _productRepository.getProducts();
    if (products.isEmpty) {
      return List.empty();
    }

    return products;
  }
}

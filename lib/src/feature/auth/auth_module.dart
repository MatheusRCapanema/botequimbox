import 'package:botequimbox/src/feature/auth/repository/auth_repository.dart';
import 'package:botequimbox/src/feature/auth/usecase/auth_usecase.dart';
import 'package:botequimbox/src/feature/auth/view/page/email_sent_page.dart';
import 'package:botequimbox/src/feature/auth/view/page/recover_password_page.dart';
import 'package:botequimbox/src/feature/auth/view/page/sign_in_page.dart';
import 'package:botequimbox/src/feature/auth/view/page/sign_up_page.dart';
import 'package:botequimbox/src/feature/auth/view/page/user_registred_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'common/api/auth_api.dart';

class AuthModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.factory((authApi) => AuthApi()),
    Bind.factory((authRepository) => AuthRepository()),
    Bind.factory((authUseCase) => AuthUseCase()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(
      '/sign-in',
      child: (BuildContext context, args) => const SignInPage(),
      children: [],
    ),
    ChildRoute(
      '/sign-up',
      child: (BuildContext context, args) => const SignUpPage(),
      children: [],
    ),
    ChildRoute(
      '/reset-password',
      child: (BuildContext context, args) => const RecoverPasswordPage(),
      children: [],
    ),
    ChildRoute(
      '/email-sent',
      child: (BuildContext context, args) => const EmailSentPage(),
    ),
    ChildRoute(
      '/user-registered',
      child: (BuildContext context, args) => const UserRegisteredPage(),
    ),
  ];
}

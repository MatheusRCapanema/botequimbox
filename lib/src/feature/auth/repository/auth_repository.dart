import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../common/api/dio_exception.dart';
import '../common/api/auth_api.dart';
import '../common/dto/register_dto.dart';
import '../common/dto/user_created_dto.dart';
import '../common/dto/user_logged_dto.dart';

class AuthRepository {
  final AuthApi _authApi = Modular.get<AuthApi>();

  Future<UserLoggedDto> authenticate(String username, String password) async {
    try {
      final response = await _authApi.authentication(username, password);
      if (response.statusCode != 200) {
        // @TODO Alert Bad credentials
      }

      return UserLoggedDto.fromJson(response.data);
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      throw errorMessage;
    }
  }

  Future<UserCreatedDto> register(RegisterDTO registerDTO) async {
    try {
      final response = await _authApi.register(registerDTO);

      if (response.statusCode != 200) {
        // @TODO Alert Bad credentials
      }

      return UserCreatedDto.fromJson(response.data);
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      throw errorMessage;
    }
  }
}

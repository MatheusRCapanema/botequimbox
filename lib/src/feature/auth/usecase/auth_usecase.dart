import 'package:botequimbox/src/common/constant/app_constant.dart';
import 'package:botequimbox/src/feature/auth/repository/auth_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../common/shared/local_storage.dart';
import '../common/dto/register_dto.dart';
import '../common/dto/user_created_dto.dart';
import '../common/dto/user_logged_dto.dart';

class AuthUseCase {
  final AuthRepository _authRepository = Modular.get<AuthRepository>();
  final LocalFlutterSecureStorage _storage =
      Modular.get<LocalFlutterSecureStorage>();

  Future<UserLoggedDto?> authenticate(String username, String password) async {
    final UserLoggedDto userModel =
        await _authRepository.authenticate(username, password);
    if (userModel.id != null) {
      await _storage.save(Constants.accessTokenKey, userModel.accessToken);
      await _storage.save(Constants.refreshTokenKey, userModel.refreshToken);
      Modular.to.navigate('/home/');

      return userModel;
    }

    return null;
  }

  Future<UserCreatedDto?> register(RegisterDTO registerDTO) async {
    final UserCreatedDto userModel =
        await _authRepository.register(registerDTO);
    if (userModel.id != null) {
      Modular.to.popAndPushNamed('/auth/user-registered');
      return userModel;
    }

    return null;
  }

  Future<void> logout() async {
    await _storage.deleteAll();
    Modular.to.navigate('/auth/sign-in');
  }
}

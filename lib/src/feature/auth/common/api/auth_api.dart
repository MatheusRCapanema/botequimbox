import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/api/dio_client.dart';
import '../../../../common/api/endpoints.dart';
import '../dto/register_dto.dart';

class AuthApi {
  final DioClient _dioClient = Modular.get<DioClient>();

  Future<Response> register(RegisterDTO registerDTO) async {
    try {
      final Response response = await _dioClient.post(
        '${Endpoints.authV1}/register',
        data: {
          "cnpj": registerDTO.cnpj,
          "email": registerDTO.email,
          "type": registerDTO.type,
          "password": registerDTO.password,
        },
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> authentication(String username, String password) async {
    try {
      final Response response = await _dioClient.post(
        '${Endpoints.authV1}/authenticate',
        data: {
          'username': username,
          'password': password,
        },
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> getUserTypeList() async {
    try {
      final Response response = await _dioClient.get(
        '${Endpoints.usersV1}/types',
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> getUserRoleList() async {
    try {
      final Response response = await _dioClient.get(
        '${Endpoints.usersV1}/roles',
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}

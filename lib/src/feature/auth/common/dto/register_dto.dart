class RegisterDTO {
  String cnpj;
  String email;
  String type;
  String password;

  RegisterDTO({
    required this.cnpj,
    required this.email,
    required this.type,
    required this.password,
  });
}

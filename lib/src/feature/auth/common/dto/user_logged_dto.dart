class UserLoggedDto {
  int? id;
  String? cnpj;
  String? email;
  String? type;
  List<dynamic>? roles;
  String? accessToken;
  String? refreshToken;

  UserLoggedDto({
    this.id,
    this.email,
    this.cnpj,
    this.type,
    this.roles,
    this.accessToken,
    this.refreshToken,
  });

  UserLoggedDto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    cnpj = json['cnpj'];
    type = json['type'];
    roles = json['roles'];
    accessToken = json['accessToken'];
    refreshToken = json['refreshToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['email'] = email;
    data['cnpj'] = cnpj;
    data['type'] = type;
    data['roles'] = roles;
    data['accessToken'] = accessToken;
    data['refreshToken'] = refreshToken;

    return data;
  }
}

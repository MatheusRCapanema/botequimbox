class UserCreatedDto {
  int? id;
  String? cnpj;
  String? email;
  String? type;
  List<dynamic>? roles;
  String? createdAt;
  String? updatedAt;

  UserCreatedDto({
    this.id,
    this.email,
    this.cnpj,
    this.type,
    this.roles,
    this.createdAt,
    this.updatedAt,
  });

  UserCreatedDto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    cnpj = json['cnpj'];
    type = json['type'];
    roles = json['roles'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['email'] = email;
    data['cnpj'] = cnpj;
    data['type'] = type;
    data['roles'] = roles;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;

    return data;
  }
}

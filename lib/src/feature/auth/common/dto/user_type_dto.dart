class UserTypeDto {
  String name;
  String description;

  UserTypeDto({required this.name, required this.description});
}

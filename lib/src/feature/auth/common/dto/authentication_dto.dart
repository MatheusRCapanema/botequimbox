class AuthenticationDTO {
  String username;
  String password;

  AuthenticationDTO({required this.username, required this.password});
}

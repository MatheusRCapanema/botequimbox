import 'package:botequimbox/src/feature/auth/view/widget/sign_up_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../common/size_config.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        leading: const BackButton(color: Colors.black),
        titleTextStyle: TextStyle(
            color: const Color(0XFF8B8B8B),
            fontSize: getProportionateScreenWidth(15),
            fontFamily: "Muli"),
        title: const Text(
          "Cadastro",
        ),
        centerTitle: true,
      ),
      body: const SafeArea(
        child: SingleChildScrollView(child: SignUpBody()),
      ),
    );
  }
}

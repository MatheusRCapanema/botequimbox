import 'package:botequimbox/src/feature/auth/view/widget/recover_password_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../common/size_config.dart';

class RecoverPasswordPage extends StatelessWidget {
  const RecoverPasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        leading: const BackButton(color: Colors.black),
        titleTextStyle: TextStyle(
            color: const Color(0XFF8B8B8B),
            fontSize: getProportionateScreenWidth(15),
            fontFamily: "Muli"),
        title: const Text(
          "Esqueci a Senha",
        ),
        centerTitle: true,
      ),
      body: const RecoverPasswordBody(),
    );
  }
}

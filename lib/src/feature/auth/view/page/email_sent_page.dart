import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../common/size_config.dart';
import '../widget/email_sent_body.dart';

class EmailSentPage extends StatelessWidget {
  const EmailSentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        leading: const BackButton(color: Colors.black),
        titleTextStyle: TextStyle(
            color: const Color(0XFF8B8B8B),
            fontSize: getProportionateScreenWidth(15),
            fontFamily: "Muli"),
        title: const Text(
          "Envio Email",
        ),
        centerTitle: true,
      ),
      body: const EmailSentBody(),
    );
  }
}

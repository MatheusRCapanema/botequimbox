import 'package:botequimbox/src/common/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../widget/sign_in_body.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        titleTextStyle: TextStyle(
          color: const Color(0XFF8B8B8B),
          fontSize: getProportionateScreenWidth(15),
          fontFamily: "Muli",
        ),
        title: const Text(
          "Sign In",
        ),
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: const SafeArea(
        child: SingleChildScrollView(child: SignInBody()),
      ),
    );
  }
}

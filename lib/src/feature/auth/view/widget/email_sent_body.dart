import 'package:botequimbox/src/common/default_button.dart';
import 'package:botequimbox/src/common/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class EmailSentBody extends StatelessWidget {
  const EmailSentBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: SizeConfig.screenHeight * 0.001,
        ),
        Image.asset(
          "lib/assets/images/success.png",
          height: SizeConfig.screenHeight * 0.5,
        ),
        SizedBox(
          height: SizeConfig.screenHeight * 0.05,
        ),
        Text(
          "Enviado com Sucesso",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(24),
            fontWeight: FontWeight.bold,
            fontFamily: 'Muli',
            color: Colors.black,
          ),
        ),
        const Spacer(),
        SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          child: DefaultButton(
              text: "Voltar para login",
              press: () {
                Modular.to.popAndPushNamed("/auth/sign-in");
              }),
        ),
        const Spacer(),
      ],
    );
  }
}

import 'package:botequimbox/src/common/size_config.dart';
import 'package:botequimbox/src/feature/auth/view/widget/sign_up_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/style_constants.dart';

class SignUpBody extends StatelessWidget {
  const SignUpBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(20),
            ),
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text(
                  "Bem-vindo!",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: getProportionateScreenWidth(28),
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Muli',
                  ),
                ),
                const Text(
                  "Criar uma nova conta para navegar no app",
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.08,
                ),
                const SignUpForm(),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                InkWell(
                  onTap: () {
                    Modular.to.pushNamed("/auth/sign-in");
                  },
                  child: GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Não possui uma conta?",
                          style: TextStyle(
                            fontSize: getProportionateScreenWidth(16),
                            fontFamily: 'Muli',
                          ),
                        ),
                        Text(
                          "Entrar",
                          style: TextStyle(
                            fontSize: getProportionateScreenWidth(16),
                            fontFamily: 'Muli',
                            color: oPrimaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

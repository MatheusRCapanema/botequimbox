import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/default_button.dart';
import '../../../../common/size_config.dart';
import '../../../../common/style_constants.dart';
import '../../usecase/auth_usecase.dart';
import 'custom_suffix_icon.dart';
import 'form_error.dart';

class SignForm extends StatefulWidget {
  const SignForm({super.key});

  @override
  State<SignForm> createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  final AuthUseCase _authUseCase = Modular.get<AuthUseCase>();
  late String cnpj;
  late String password;
  bool remember = false;
  final List<String> errors = [];

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          cnpjInputFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          passwordInputFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          Row(
            children: [
              Checkbox(
                value: remember,
                activeColor: oPrimaryColor,
                onChanged: (value) {
                  setState(() {
                    remember = value!;
                  });
                },
              ),
              const Text("Remember me"),
              const Spacer(),
              InkWell(
                onTap: () {
                  Modular.to.pushNamed("/auth/reset-password");
                },
                child: GestureDetector(
                  child: const Text(
                    "Esqueci a senha?",
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                ),
              ),
            ],
          ),
          FormError(errors: errors),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          DefaultButton(
            text: "Entrar",
            disabled: errors.isNotEmpty,
            press: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState?.save();
                _authUseCase.authenticate(cnpj, password);
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField passwordInputFieldMethod() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue!,
      onChanged: removePasswordError,
      validator: passwordValueValidator,
      decoration: InputDecoration(
        labelText: "Senha",
        hintText: "Digite sua senha",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        suffixIcon: const CustomSuffixIcon(
          svgIcon: "lib/assets/icons/Lock.svg",
        ),
      ),
    );
  }

  TextFormField cnpjInputFieldMethod() {
    return TextFormField(
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
        CnpjInputFormatter(),
      ],
      onSaved: (newValue) => cnpj = newValue!,
      onChanged: removeCnpjError,
      validator: cnpjValueValidator,
      decoration: InputDecoration(
        labelText: "CNPJ",
        hintText: "Digite seu CNPJ",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        suffixIcon: const CustomSuffixIcon(
          svgIcon: "lib/assets/icons/User.svg",
        ),
      ),
    );
  }

  void addError({required String error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({required String error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  String? passwordValueValidator(value) {
    if (!errors.contains(kPassNullError) && value!.isEmpty) {
      addError(error: kPassNullError);

      return "";
    } else if (value!.length < 8 &&
        value!.length > 1 &&
        !errors.contains(kShortPassError)) {
      addError(error: kShortPassError);

      return "";
    }

    return null;
  }

  void removePasswordError(value) {
    if (errors.contains(kPassNullError) && value.isNotEmpty) {
      removeError(error: kPassNullError);
    } else if (value.length >= 8) {
      removeError(error: kShortPassError);
    }
  }

  void removeCnpjError(value) {
    if (errors.contains(kCnpjNullError) && value.isNotEmpty) {
      removeError(error: kCnpjNullError);
    } else if (value.length == 18 && errors.contains(kInvalidCnpjError)) {
      removeError(error: kInvalidCnpjError);
    }
  }

  String? cnpjValueValidator(value) {
    if (!errors.contains(kCnpjNullError) && value!.isEmpty) {
      addError(error: kCnpjNullError);

      return "";
    } else if ((value!.length > 1 && value!.length < 18) &&
        !errors.contains(kInvalidCnpjError)) {
      addError(error: kInvalidCnpjError);
    }

    return null;
  }
}

import 'package:botequimbox/src/feature/auth/common/dto/user_type_dto.dart';
import 'package:botequimbox/src/feature/auth/usecase/auth_usecase.dart';
import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/default_button.dart';
import '../../../../common/size_config.dart';
import '../../../../common/style_constants.dart';
import '../../common/dto/register_dto.dart';
import 'custom_suffix_icon.dart';
import 'form_error.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  final AuthUseCase _authUseCase = Modular.get<AuthUseCase>();
  late String cnpj;
  late String email;
  late String password;
  late UserTypeDto type = userTypes.first;
  bool remember = false;
  final List<String> errors = [];

  List<UserTypeDto> userTypes = [
    UserTypeDto(name: "TYPE_RETAILER", description: "VAREJISTA"),
    UserTypeDto(name: "TYPE_MANUFACTURER", description: "FABRICANTE"),
  ];

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          cnpjInputFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          emailInputFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          typeDropdownFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          passwordInputFieldMethod(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          FormError(errors: errors),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          DefaultButton(
            text: "Cadastrar",
            disabled: errors.isNotEmpty,
            press: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState?.save();
                if (errors.isEmpty) {
                  _authUseCase.register(RegisterDTO(
                    cnpj: cnpj,
                    email: email,
                    type: type.name,
                    password: password,
                  ));
                  // Modular.to.navigate("/home");
                }
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField passwordInputFieldMethod() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue!,
      onChanged: removePasswordError,
      validator: passwordValueValidator,
      decoration: InputDecoration(
        labelText: "Senha",
        hintText: "Digite sua senha",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        suffixIcon: const CustomSuffixIcon(
          svgIcon: "lib/assets/icons/Lock.svg",
        ),
      ),
    );
  }

  TextFormField cnpjInputFieldMethod() {
    return TextFormField(
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
        CnpjInputFormatter(),
      ],
      onSaved: (newValue) => cnpj = newValue!,
      onChanged: removeCnpjError,
      validator: cnpjValueValidator,
      decoration: InputDecoration(
        labelText: "CNPJ",
        hintText: "Digite seu CNPJ",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        suffixIcon: const CustomSuffixIcon(
          svgIcon: "lib/assets/icons/User.svg",
        ),
      ),
    );
  }

  TextFormField emailInputFieldMethod() {
    return TextFormField(
      inputFormatters: const [],
      onSaved: (newValue) => email = newValue!,
      onChanged: removeEmailError,
      validator: emailValueValidator,
      decoration: InputDecoration(
        labelText: "E-MAIL",
        hintText: "Digite seu e-mail",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: const BorderSide(color: kTextColor),
          gapPadding: 10,
        ),
        suffixIcon: const CustomSuffixIcon(
          svgIcon: "lib/assets/icons/Mail.svg",
        ),
      ),
    );
  }

  DropdownButton typeDropdownFieldMethod() {
    return DropdownButton<UserTypeDto>(
      value: type,
      onChanged: (UserTypeDto? value) {
        setState(() {
          type = value!;
        });
      },
      items: userTypes.map((userTypeModel) {
        return DropdownMenuItem<UserTypeDto>(
          value: userTypeModel,
          child: Text(userTypeModel.description),
        );
      }).toList(),
      style: const TextStyle(fontSize: 18, color: Colors.black),
      icon: const Icon(Icons.arrow_drop_down_rounded),
      iconSize: 36,
      dropdownColor: Colors.white,
      borderRadius: BorderRadius.circular(10),
    );
  }

  void addError({required String error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({required String error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  void removeCnpjError(value) {
    if (errors.contains(kCnpjNullError) && value.isNotEmpty) {
      removeError(error: kCnpjNullError);
    } else if (value.length == 18 && errors.contains(kInvalidCnpjError)) {
      removeError(error: kInvalidCnpjError);
    }
  }

  String? cnpjValueValidator(value) {
    if (!errors.contains(kCnpjNullError) && value!.isEmpty) {
      addError(error: kCnpjNullError);

      return "";
    } else if ((value!.length > 1 && value!.length < 18) &&
        !errors.contains(kInvalidCnpjError)) {
      addError(error: kInvalidCnpjError);
    }

    return null;
  }

  void removeEmailError(value) {
    if (errors.contains(kEmailNullError) && value.isNotEmpty) {
      removeError(error: kEmailNullError);
    } else if (value.length == 18 && errors.contains(kInvalidEmailError)) {
      removeError(error: kInvalidEmailError);
    }
  }

  String? emailValueValidator(value) {
    if (!errors.contains(kEmailNullError) && value!.isEmpty) {
      addError(error: kEmailNullError);

      return "";
    } else if ((value!.length > 1 && value!.length < 18) &&
        !errors.contains(kInvalidEmailError)) {
      addError(error: kInvalidEmailError);
    }

    return null;
  }

  String? passwordValueValidator(value) {
    if (!errors.contains(kPassNullError) && value!.isEmpty) {
      addError(error: kPassNullError);

      return "";
    } else if (value!.length < 8 &&
        value!.length > 1 &&
        !errors.contains(kShortPassError)) {
      addError(error: kShortPassError);

      return "";
    }

    return null;
  }

  void removePasswordError(value) {
    if (errors.contains(kPassNullError) && value.isNotEmpty) {
      removeError(error: kPassNullError);
    } else if (value.length >= 8) {
      removeError(error: kShortPassError);
    }
  }
}

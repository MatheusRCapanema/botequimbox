import 'package:botequimbox/src/feature/details/widget/body.dart';
import 'package:flutter/material.dart';

import '../../../../common/rounded_icon_button.dart';
import '../../widget/custom_app_bar.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final ProductDetailsArgument? arguments =
        ModalRoute.of(context)!.settings.arguments as ProductDetailsArgument?;

    return Scaffold(
      backgroundColor: const Color(0xFFFF5F6F9),
      appBar: CustomAppBar(arguments!),
      body: Body(
        product: arguments.product,
      ),
    );
  }
}

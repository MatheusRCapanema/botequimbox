import 'package:botequimbox/src/common/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../common/rounded_icon_button.dart';

class CustomAppBar extends PreferredSize {
  final ProductDetailsArgument arguments;

  CustomAppBar(this.arguments, {super.key})
      : super(
            preferredSize: Size.fromHeight(AppBar().preferredSize.height),
            child: AppBarContent(rating: arguments.product.rating ?? 0.0));
}

class AppBarContent extends StatelessWidget {
  final double rating;

  const AppBarContent({super.key, required this.rating});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Center(
              child: RoundedIconButton(
                iconData: Icons.arrow_back_ios,
                press: () => Navigator.pop(context),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14),
              ),
              child: Row(
                children: [
                  Text(
                    rating.toString(),
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(width: 5),
                  SvgPicture.asset("lib/assets/icons/Star Icon.svg")
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

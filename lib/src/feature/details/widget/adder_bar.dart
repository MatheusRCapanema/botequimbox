import 'package:botequimbox/src/feature/home/common/dto/product_dto.dart';
import 'package:flutter/material.dart';

import '../../../common/rounded_icon_button.dart';
import '../../../common/size_config.dart';
import '../../../common/style_constants.dart';

class SizeSelector extends StatefulWidget {
  const SizeSelector({
    Key? key,
    required this.product,
  }) : super(key: key);

  final ProductDto product;

  @override
  _SizeSelectorState createState() => _SizeSelectorState();
}

class _SizeSelectorState extends State<SizeSelector> {
  int selectedSize = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        children: [
          ...List.generate(
            widget.product.size!.length,
            (index) => SizeOption(
              text: widget.product.size![index],
              isSelected: index == selectedSize,
              onPressed: () {
                setState(() {
                  selectedSize = index;
                });
              },
            ),
          ),
          const Spacer(),
          RoundedIconButton(
            iconData: Icons.remove,
            press: () {},
          ),
          SizedBox(width: getProportionateScreenWidth(20)),
          RoundedIconButton(
            iconData: Icons.add,
            showShadow: true,
            press: () {},
          ),
        ],
      ),
    );
  }
}

class SizeOption extends StatelessWidget {
  const SizeOption({
    Key? key,
    required this.text,
    required this.isSelected,
    required this.onPressed,
  }) : super(key: key);

  final String text;
  final bool isSelected;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        margin: const EdgeInsets.only(right: 2),
        padding: EdgeInsets.all(getProportionateScreenWidth(8)),
        height: getProportionateScreenWidth(40),
        width: getProportionateScreenWidth(55),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: isSelected ? oPrimaryColor : Colors.transparent,
          ),
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            text,
            style: const TextStyle(
              fontSize: 12,
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:botequimbox/src/common/size_config.dart';
import 'package:botequimbox/src/common/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/constant/app_constant.dart';
import '../../../../common/default_button.dart';
import '../../../../common/shared/local_storage.dart';
import 'onboard_content.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final LocalFlutterSecureStorage _storage =
      Modular.get<LocalFlutterSecureStorage>();
  int currentPage = 0;
  final PageController _pageController = PageController();

  List<Map<String, String>> onboardData = [
    {
      "text": "Bem-vindo ao BotequimBox, Vamos encher o estoque!",
      "image": "lib/assets/images/onboard1.png"
    },
    {
      "text": "Nós ajudamos você e seu negócio a comprar novos produtos!",
      "image": "lib/assets/images/onboard2.png"
    },
    {
      "text": "Venha fazer parte do time BotequimBox",
      "image": "lib/assets/images/onboard3.png"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: PageView.builder(
                controller: _pageController,
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: onboardData.length,
                itemBuilder: (context, index) {
                  String? image = onboardData[index]["image"];
                  String? text = onboardData[index]["text"];

                  return OnboardingContent(
                    image: image ?? "",
                    text: text ?? "",
                  );
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        onboardData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    const Spacer(flex: 3),
                    DefaultButton(
                      text: "Continue",
                      press: () async {
                        if (currentPage == onboardData.length - 1) {
                          // Se estiver na última página, navegue para a tela de login
                          await _storage.save(
                              Constants.ourConditionKey, "true");
                          Modular.to.navigate("/auth/sign-in");
                        } else {
                          // Caso contrário, vá para a próxima página do onboarding
                          _pageController.nextPage(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeIn,
                          );
                        }
                      },
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({required int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: const EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? oPrimaryColor : const Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}

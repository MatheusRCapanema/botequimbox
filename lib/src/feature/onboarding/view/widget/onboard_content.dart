import 'package:flutter/material.dart';

import '../../../../common/size_config.dart';
import '../../../../common/style_constants.dart';

class OnboardingContent extends StatelessWidget {
  const OnboardingContent({
    super.key,
    required this.text,
    required this.image,
  });

  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const Spacer(),
        Text(
          "BOTEQUIMBOX",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(36),
            color: oPrimaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: kTextColor,
            ),
          ),
        ),
        const Spacer(flex: 2),
        Image.asset(
          image,
          height: getProportionateScreenHeight(265),
          width: getProportionateScreenWidth(235),
        ),
      ],
    );
  }
}

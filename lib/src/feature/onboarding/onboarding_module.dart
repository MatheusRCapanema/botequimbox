import 'package:botequimbox/src/feature/onboarding/view/page/onboarding_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';

class OnboardingModule extends Module {
  @override
  List<Bind> get binds => const [];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Modular.initialRoute,
          child: (BuildContext context, args) => const OnboardingPage(),
        ),
      ];
}

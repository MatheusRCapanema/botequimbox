import 'package:botequimbox/src/feature/splash/view/page/splash_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashModule extends Module {
  @override
  List<Bind> get binds => const [];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Modular.initialRoute,
          child: (BuildContext context, args) => const SplashPage(),
        ),
      ];
}

import 'dart:async';

import 'package:botequimbox/src/common/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../common/constant/app_constant.dart';
import '../../../../common/shared/local_storage.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final LocalFlutterSecureStorage _storage =
      Modular.get<LocalFlutterSecureStorage>();
  bool _acceptedOurCondition = false;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () {
      _getOurConditionValue();

      Modular.to.navigate('/onboarding/');
    });
  }

  Future<void> _getOurConditionValue() async {
    final ourConditionValue = await _storage.get(Constants.ourConditionKey);
    setState(() {
      _acceptedOurCondition = ourConditionValue == null ? false : true;
    });

    _navigateToAuth();
  }

  void _navigateToAuth() {
    if (_acceptedOurCondition) {
      Modular.to.navigate('/auth/sign-in');
    }

    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: oPrimaryColor,
      appBar: null,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              "BotequimBox",
              style: TextStyle(
                color: Colors.white,
                fontSize: 44.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
